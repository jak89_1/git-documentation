# GIT Tutorial

[What is Git?](Readme.md#what-is-git)\
[How to get Git?](Readme.md#how-to-get-git)\
[The most common Commands](Readme.md#the-most-common-commands)\
[What? You want more?!](Readme.md#what-you-want-more)


## What is Git?
Git is a type of version control system (VCS).
that makes it easier to track changes to files. For example,
when you edit a file, git can help you determine exactly what changed,
who changed it, and why.

It’s useful for coordinating work among multiple people on a project,
and for tracking progress over time by saving “checkpoints”.
You could use it while writing an essay (like i currently do with this doc),
or to track changes to artwork and design files.

If you start to understand how git works,
you’ll see why conflicts occur and how to recover from these situations easily.


## How to get Git?
Most systems already have git as default.
You can download git’s command-line interface
(CLI)[here](https://git-scm.com/downloads).
i recommend to use the command-line, not all those GUI tools and IDE plugins!


## The most common Commands
### init
```
git init
```
This will create a hidden `.git` folder inside your current folder — this is
the "repository" (or repo) where git stores all of its internal tracking data.
Any changes you make to any files within the original folder will now be
possible to track.

The original folder is now referred to your working directory,
as opposed to the repository (the .git folder) that tracks your changes.
You work in the working directory.


### clone
```
git clone https://gitlab.com/jak89_1/git-documentation.git
```
This will download a .git repository from the internet (Gitlab)
to your computer and extract the latest snapshot of the repo (all the files)
to your working directory. By default it will all be saved in a folder with the
same name as the repo (in this case `git-documentation`).

The URL you specify here is called the remote origin
(the place where the files were originally downloaded from).
This term will be used later on.


### status
```
git status
```
This will print some basic information,
such as which files have recently been modified.

You should check your status anytime you’re confused and before you commit.
Git will print additional information depending on what’s currently going on in
order to help you out.


### branch
```
git branch <new-branch-name>
```
You can think of this like creating a local “checkpoint”
(technically called a reference) and giving it a name.
It’s similar to doing File > Save as… in a text editor.
the new created branch is a reference to the current state of your repo.
The branch name can then be used in various other commands

Similar to branching, more commonly you will save each checkpoint as you go
along in the form of commits (see git commit).

Commits are a particular type of checkpoint called a revision.
The name will be a random-looking hash of numbers and letters such as e093542.
This hash can then be used in various other commands just like branch names.

That’s really the core function of git: To save checkpoints (revisions)
and share them with other people. Everything revolves around this concept.


### add
```
git add <files>
```
After editing some files, this command will mark any changes you’ve made as
“staged” (or “ready to be committed”).
⚠️ If you then go and make more changes,
those new changes will not automatically be staged,
even if you’ve changed the same files as before.
This is useful for controlling exactly what you commit,
but also a major source of confusion for newcomers.

If you’re ever unsure, just type git status again to see what’s going on.
You’ll see\
“Changes to be committed:” followed by file names in green.\
Below that you’ll see\
“Changes not staged for commit:” followed by file names in red.\
These are not yet staged.\
if you simply want to add all do
```
git add .
```
or
```
git add --all
```
(dont use \* if you have ignored files!)


### commit
```
git commit
```
This will open your default command-line text editor and ask you to type
in a commit message.
As soon as you save and quit, your commit will be saved locally.
The commit message is important to help other people understand what was
changed and why you changed it.
You can use the -m flag as a shortcut to write a message. For example:
```
git commit -m "added new feature XYZ"
```

### push
```
git push origin <branch-name>
```
This will upload your local branch to the remote named origin
(remember, that’s the URL defined initially during clone).

After a successful push, your teammates will then be able to pull your branch
to view your commits (see git pull below).

As a shortcut, you can type the word `HEAD` instead of `branch-name` to
automatically use the branch you’re currently on.
`HEAD` always refers to your latest checkpoint,
that is, the latest commit on your current branch.

As mentioned earlier, everything in git can be thought of as a checkpoint.
Here’s a list of the types of checkpoint you know about now
(again, these are technically called “references” and “revisions”):

`HEAD` -> is the reference to your current branch\
`<branch-name>` the default branch is named: `master`\
`<commit-hash>` is a unique hash, to identify your commit.
`e093542d01d11c917c316bfaffd6c4e5633aba58` (or `e093542` for short)\
`<tag-name>` easy versioning, like `v1.0.0`

Finally, special characters like ^, ~, and @{} can be used to modify references.

### pull
```
git pull origin <branch-name>
```
this command gets all the content of the remote repository to your working
directory.
so you can work always with the newest version.


### fetch
```
git fetch origin
```
this does the same like pull, but it only updates the hidden folder `.git`,
all files you are working on dont get touched.


### checkout
```
git checkout <branch-name>
```
with checkout, you can change your working branch, with `-b branch-name`
you create a new branch, and change in to it.


### merge (soon)
<!-- TODO -->
### rebase (soon)
<!-- TODO -->

### cherry-pick
```
git cherry-pick COMMIT-HASH-HERE
```
cherry-picking is great if you have much branches, and you want just single
commits on your working branch, so you can pick them with there hashes from
other branches even from other remotes is working.


### log
```
git log
```
shows you the commit log with commit-hash, author, date and commit message you
can add `-1` if you only want the last one, -2, -3 and so one, depends how much
commits you want or need.



## config
There are some settings you can change to make your live easier.
some are also required like:
```
git config user.name "Foo Bar"
```
```
git config user.email "foo@bar.baz"
```
you can set this values also global with the modifier `--global`

if you want to use your favorite editors for commit messages:
```
git config --global core.editor emacs
```
(i just use the default 'vi' or 'vim', you can use emacs or even gedit
the best would be an editor with syntax-highlighting,
this one also shows how you can use the `--global` modifier)

you can find all other configuration options in the manual page:
```
man git-config
```

## Advanced Git

### Signing you Commits
first of all, lets list if there are already keys installed with:
```
gpg --list-keys
```
this should print out something like this:
```
/Users/foobar/.gnupg/pubring.gpg
---------------------------------
pub   2048R/0A46826A 2020-06-01
uid                  Foo Bar (Git signing key) <foo@bar.baz>
sub   2048R/874529A9 2020-06-01
```
If you don’t have a key installed, you can generate one with:
```
gpg --gen-key
```
then add the fingerprint to your git configuration
(this is the key you will find in the `gpg --list-keys` command. see above)
```
git config --global user.signingkey 0A46826A
```
now you can simply add `-S` to all your commits, and sign them.
(note: some repository's only allow signed commits!)
if you dont want to use the `-S` option every time,
you can setup to use it always with:
```
git config commit.gpgsign true
```

## What? You want more?!

[git-scm](https://git-scm.com/book/en/v2)\
[advanced guide](http://think-like-a-git.net/)\
[Wiki](https://en.wikipedia.org/wiki/Git)\
[stackoverflow](https://stackoverflow.com/questions/tagged/git)
